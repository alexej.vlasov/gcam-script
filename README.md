# Motivation

This script is designed to handle (actually delete) multiple subfolders at the DCIM folder created when Google Camera is used. 
By default if you are taken Portrait or other GCam pictures required special post-processing the multiple folders are created in the DCIM folder. 
This creates a lot of mess when using Gallery software or copying files to PC.

# Usage
Just run this script on your android phone using apropriate tool. I use SManager.

# How does this work
This is pretty simple script. It creates UNBLURRED folder at the top level and move all NON_BLURRED images there (for the purpose if you still like use them). All blurred images are moving to one level up - Camera folder.
It's usual place for pictures you are taken. And then script is trying to delete subfolders if they empty. If subfolders still have some files they will keep untocuhed.