#!/usr/bin/env bash

#
#rm -r target
#mkdir target
#cp -R orig/* target/

WORKDIR=/sdcard/DCIM/Camera
#WORKDIR=/Users/alexey_vlasov/lexas/gcam-tool/target/DCIM/Camera/

mkdir $WORKDIR/../NON_BLURED/

mv $WORKDIR/IMG*/*COVER.jpg  $WORKDIR/
mv $WORKDIR/IMG*/*.jpg $WORKDIR/../NON_BLURED/
cd $WORKDIR
find . -type d -name "IMG*" -execdir echo `pwd` '{}' \;
find . -type d -name "IMG*" -execdir rmdir {} \;
echo 'Done!'
